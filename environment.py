#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jul 28 15:21:08 2017

@author: magdadubois
"""

from grid_file import Grid
from agent import Agent
from parameters import *

import torch
from torch.autograd import Variable
import numpy as np

#######################
### The Environment ###
#######################

class Environment:
    def __init__(self):
        self.grid = Grid()
        self.agent = Agent()
    
    def reset(self):
        """Start a new episode by resetting grid and agent"""
        self.grid.reset()
        self.agent.reset()
        self.agent.pos = pos_ini
        self.agent.cont_pos = Variable(torch.from_numpy(np.array([pos_ini[0],pos_ini[1]])))
        self.agent.pos_old = pos_ini
        
        #self.t = 0
        self.history = []
        self.record_step()
        
        # Get reward from where agent is
        value = self.grid.ng[self.agent.pos]
        self.grid.ng[self.agent.pos] = REW_AFTER_VISIT # so that does not go backwards
        self.grid.ng_previous_position[self.agent.pos]= REW_AFTER_VISIT
        
        return self.visible_state, self.visible_state_scaled
    
    def record_step(self):
        """Add the current state to history for display later"""
        #grid = np.array(self.grid.grid)
        grid = np.array(self.grid.ng)
        #visible = np.array(self.grid.visible(self.agent.pos))
        # pas besoin de mettre pos_cont car les deplacements sont stockes dans grid
        visible = np.array(self.grid.visible_interpolated(self.agent.pos))
        visible_scaled = np.array(self.grid.visible_interpolated_scaled(self.agent.pos))
        visible_larger = np.array(self.grid.visible_interpolated_larger(self.agent.pos))
        visible_previous_pos=np.array(self.grid.visible_interpolated_previous_pos(self.agent.pos))
        self.history.append((grid, visible, visible_scaled, visible_larger, self.agent.reward, visible_previous_pos))
    
    @property
    def visible_state(self):
        """Return the visible area surrounding the agent, and current agent health"""
        return self.grid.visible(self.agent.pos)
    
    @property
    def visible_state_interpol(self):
        """Return the visible area surrounding the agent, and current agent health"""
        
        pixel_y, pixel_x = self.agent.pos
        
        ycont = self.agent.cont_pos.data[0]
        xcont = self.agent.cont_pos.data[1]
        
        #we will interpolate with this displacement
        self.grid.y_disp = ycont-pixel_y
        self.grid.x_disp = xcont-pixel_x
        
        # Pas besoin de passer cont_pos, les deplacement sont stockes dans grid
        return self.grid.visible_interpolated(self.agent.pos)
    
    @property
    def visible_state_interpol_larger(self):
        """Return the visible area surrounding the agent, and current agent health"""
        return self.grid.visible_interpolated_larger(self.agent.pos)
    
    @property
    def visible_state_interpol_scaled(self):
        """Return the visible area surrounding the agent, and current agent health"""
        return self.grid.visible_interpolated_scaled(self.agent.pos)
    
    @property
    def visible_state_scaled(self):
        """Return the visible area surrounding the agent, and current agent health"""
        return self.grid.visible_scaled(self.agent.pos)
    
    @property
    def visible_state_larger(self):
        """Return the visible area surrounding the agent, and current agent health"""
        return self.grid.visible_larger(self.agent.pos)
    
    @property
    def visible_state_previous_pos(self):
        """Return the visible area surrounding the agent, and current agent health"""
        return self.grid.visible_previous_pos(self.agent.pos)
    
    @property
    def visible_state_interpol_previous_pos(self):
        """Return the visible area surrounding the agent, and current agent health"""
        return self.grid.visible_interpolated_previous_pos(self.agent.pos)
    
    def step(self, action):
        """Update state (grid and agent) based on an action"""       
        self.agent.act(action)

        # Get reward from where agent landed
        value = self.grid.grid[self.agent.pos]
                
        # Rewards at end of episode
        if value == GOAL_VALUE:
            done = 1
        elif value == EDGE_VALUE:
            x,y = self.agent.pos_old

            xnew, ynew = x + random.randint(-1,1), y + random.randint(-1,1)   #bumps randomly out of the edge
            
            while self.grid.grid[(xnew,ynew)] == EDGE_VALUE :
                xnew, ynew = x + random.randint(-1,1), y + random.randint(-1,1) #bumps randomly out of the edge
            
            self.agent.pos = int(xnew),int(ynew)
            self.agent.pos_cont = xnew,ynew
                
            done = 0
        else:
            done = 0 #Not at the end point yet
        
        self.grid.grid[self.agent.pos] = REW_AFTER_VISIT # so that does not go backwards
        self.grid.grid_previous_position[self.agent.pos]= REW_AFTER_VISIT
        self.agent.reward = value

        # Save in history
        self.record_step()
        
        # Store current pos
        self.agent.pos_old = self.agent.pos

        #return self.visible_state, self.agent.reward, done
        return self.visible_state, self.visible_state_scaled, self.visible_state_larger, self.agent.reward, done,\
        self.visible_state_previous_pos, self.visible_state_interpol
        
    def step_cont(self, action):
        """Update state (grid and agent) based on an action""" 
        self.agent.act_cont(action)
                                
        # GOAL REACHED        
        if self.agent.pos[0] >= self.grid.goal_position[0]-RADIUS and self.agent.pos[0] <= self.grid.goal_position[0]+RADIUS \
            and self.agent.pos[1] >= self.grid.goal_position[1]-RADIUS and self.agent.pos[1] <= self.grid.goal_position[1]+RADIUS : 
            done = 1
        elif self.agent.pos[0] < VISIBLE_RADIUS+1 or self.agent.pos[0] > GRID_SIZE + VISIBLE_RADIUS - 1 \
            or self.agent.pos[1] < VISIBLE_RADIUS+1 or self.agent.pos[1] > GRID_SIZE + VISIBLE_RADIUS - 1 :
            done = 1
        else:
            done = 0 
        
        value_cont = self.grid.ng[int(self.agent.pos[0]), int(self.agent.pos[1])]
        self.agent.reward = value_cont
        
        self.grid.ng[int(self.agent.pos[0]), int(self.agent.pos[1])] = REW_AFTER_VISIT # so that does not go backwards
        self.grid.ng_previous_position[self.agent.pos]= REW_AFTER_VISIT
        self.grid.grid[self.agent.pos] = REW_AFTER_VISIT 
        
        # Save in history
        self.record_step()
        
        # Store current pos
        self.agent.pos_old = self.agent.pos
        
        return self.visible_state_interpol, self.visible_state_interpol_scaled, self.visible_state_interpol_larger,\
        self.agent.reward, done, self.visible_state_interpol_previous_pos

