# coding: utf-8
import numpy as np

import torch
import torch.optim as optim
from torch.autograd import Variable

from environment import Environment
from neuralnets import ValueNet, Policy
from animate import *
from trpo import *
from advantage import *

#######################
####### Testing #######
#######################

net_test = Policy()
optimiser_pol = optim.Adam(net_test.parameters(), lr=0.001)

value = ValueNet(num_timesteps)
optimiser_val = optim.Adam(value.parameters(), lr=0.001)

loss_vector = []

for l in range(num_episodes+1):
    print("l", l)
    
    optimiser_pol.zero_grad()
    optimiser_val.zero_grad()
    
    if l%((num_episodes)/2) == 0 :
        print("episode (50%) :"), print(l)
    
    env = Environment()
    env.reset()
    
    state = np.zeros((batch_n, num_timesteps,8,8))
    state_scaled = np.zeros((batch_n, num_timesteps,8,8))
    state_prev_pos = np.zeros((batch_n, num_timesteps,8,8))
    
    action_2 = []
    action_means = []
    action_stds = []
    action_log_stds = []
    
    dones = np.zeros((batch_n, num_timesteps, 1))
    rewards = np.zeros((batch_n, num_timesteps, 1))
    min_timesteps = [num_timesteps-4]

    input_nn = torch.Tensor(batch_n,12,8,8) # Dimensions: batch, channel, filter height, filter width
    
    for t in range(4, num_timesteps):
        
        for b in range(batch_n):
            
            input_nn[b][0][:][:] = torch.FloatTensor(state[b, t-4,:,:])
            input_nn[b][1][:][:] = torch.FloatTensor(state[b, t-3,:,:])
            input_nn[b][2][:][:] = torch.FloatTensor(state[b, t-2,:,:])
            input_nn[b][3][:][:] = torch.FloatTensor(state[b, t-1,:,:])
            input_nn[b][4][:][:] = torch.FloatTensor(state_scaled[b, t-4,:,:])
            input_nn[b][5][:][:] = torch.FloatTensor(state_scaled[b, t-3,:,:])
            input_nn[b][6][:][:] = torch.FloatTensor(state_scaled[b, t-2,:,:])
            input_nn[b][7][:][:] = torch.FloatTensor(state_scaled[b, t-1,:,:])
            input_nn[b][8][:][:] = torch.FloatTensor(state_prev_pos[b, t-4,:,:])
            input_nn[b][9][:][:] = torch.FloatTensor(state_prev_pos[b, t-3,:,:])
            input_nn[b][10][:][:] = torch.FloatTensor(state_prev_pos[b, t-2,:,:])
            input_nn[b][11][:][:] = torch.FloatTensor(state_prev_pos[b, t-1,:,:])
        
        action_mean, log_std, action_std = net_test(Variable(input_nn, volatile=False))
        
        action_2.append(torch.normal(action_mean, action_std))
        action_means.append(action_mean)
        action_stds.append(action_std)
        action_log_stds.append(log_std)
        
        for b in range(batch_n):
            
            if dones[b,:,:].all() == 0:
                
                next_state, next_state_scaled, state_larger, reward, done, next_state_prev_pos = env.step_cont(action_2[0][b])
                state[b, t,:,:] = next_state
                state_scaled[b, t,:,:] = next_state_scaled
                state_prev_pos[b, t,:,:] = next_state_prev_pos
                dones[b,t,:]= done
                rewards[b,t,:]= reward
            
            else:
                min_timesteps.append(t-4)

    #Transpose them in order to have timesteps * batch and not the opposite
    t_rewards = list(map(list, zip(*rewards)))
    t_dones = list(map(list, zip(*dones)))
    
    all_advantages = comp_advantage(t_rewards, t_dones, state, value, optimiser_val)
    
    loss = 0
    
    for i in range(min(min_timesteps)):
        
        prob = normal_density(action_2[i], action_means[i], action_stds[i])
        t_prob = prob.transpose(0,1)
        loss -= torch.log(t_prob[0]) * Variable(all_advantages[i]) + torch.log(t_prob[1]) * Variable(all_advantages[i])
    
    print(loss)
    tot_loss = torch.mean(loss)
    autograd.grad(tot_loss, net_test.parameters(), create_graph=True, retain_graph=True)
    optimiser_pol.step()

if l!=0 and l%gif_frequency == 0 :
    animate(env.history)

# Probleme 1
# Parfois ne marche pas
# Ca depend des fois, pas toujours a le meme iteration
# Probablement quand touche le but ca foire au niveau des iterations
# Pour voir si c'est ca, faire un print quand touche le but et on regarde si c'est au meme endroit que ne marche pas
# NON c'est pas quand reach GOAL_VALUE
# C'est comme si la valeure etait trop petite ? Reward peut etre ? Essayer de noter les valeures
# TROUVE -> somme de random multinomial est sup > 1
# Solution : Math_floor sur un certain nombre de decimales
# Ou utilser quelque chose de plus precis
# PAS UNE TRES BONNE SOLUTION
# + solution de kai avec soft max ??
# C'etait un probleme de conversion de types, maintenant OK ...?

# Probleme 2
# A dit que si fonce contre le mur il rebondit, du coup il fait ca et reste dans la meme position
# Stuck in local minima
# Voir avec Jack ce que a fait
# la j'ai change, du coup maintenant si il fonce contre le mur, il recoit le -10 de penalite
# MAIS COINCE QUAND MEME
# Revoir comment je change la position, peut-etre que j'oublie quelque chose ??
# Le probleme c'est que si il decide de quelque chose est mieux ca ne changera plus parce que les probabilites 
# de distribution sont trop grande et donc ne va jamais selectionne les autres actions

# SOLVED : Probleme 3
# Donne une erreur quand change la valeur de edge value en -5
# Il fallait changer le -10 dans la boule en une variable

# SOLVED : Probleme 4
# A un certaint moment, dans tous les pol (pol[0][0] etc) sont egal a Nan

# Probleme 5
# Essaye d'aller vers Goal_value au lieu de maximiser le reward, il faudrait donc diminuer valeure de goal value mais
# dire que si il est dans le perimetre de la fin il est DONE (et non DONE quand a reach la value)

# Probleme 6
# On dirait qu'il evite les Goal value ?? Il y en a toujours beaucoup dans les premieres iterations, et apres plus ??

# Probleme 7
# It keeps getting stuck with a certain policy and doesn't change it 
# Solution -> TRPO

# Probleme 8
# Quand on change min_pixel value en 0.5, il y a un trait orange sur la grid 

# Probleme 9 
# Randomiser ainit

# Probleme 10
# Si je penalise le faire de revenir en arriere, il reste bloqué dans un coin 

# A FAIRE

# Down sampling et ou est deja alle
# Teste sur le network
# Rajoute truc de TRPO

# A REGLER
# num_timesteps and not len(transitions) because the input to the nn must be constant
# all_states = np.zeros((len(num_timesteps), VISIBLE_RADIUS, VISIBLE_RADIUS))

# Rajouter une ecran ou on voit la position precedente
# Reflechir si on prend toute la grid sans le neuron, et qu'on prend un ecran visible de ca, ou si on prend
# ==> Il faut prendre tout grid et ensuite une partie de ca si on veut update la position
# direct le visible au quel on sous-trait le neurone ??
# Gridworldnew-PreviousTrajectory marche pas
# Faire copie de downscale et essayer
# Rajouter 4 pas de temps au networks
# Coder TRPO

# Changé le += value en self.agent.reward = value
# C'est juste ou pas ??

# PROBLEME avec le number of input qu'on donne la fonction lineare. Pas de probleme dans PolicyNet
# PROBLEME is it OK to sum all the advantages like the rewards ?? 
# PROBLEME : in Advantage, should be returns = torch.Tensor(rewards.size(0),1)  (Alors que la on force taille = 100
# car sinon il y a une inconsistence avec value_function, pour laquelle output est force a 100)
# PROBLEME : Faire quelque chose avec la variable : num_timesteps (la on la declare globale)
# PROBLEME : Pour le network de value function, input seulement states pour l'instant, es-ce qeu devrait input 
# le downscaled one et previous positions ???
# PROBLEME : all_states a une taille constante pour l'instant. Des que arrete on met tous les states restant
# a que des 1, il faudrait faire quelque chose par rapport a ca ??


# PROBLEME : optimiser les boucles, soit avec C, soit en faisant loop direct dans les vecteurs
# Installer le truc de Kai pour voir si va plus vite
# coder TRPO
# Plot advantage au lieu de reward pour voir si est optimise ??
# Afficher advantage pour voir 
# Rajouter scaled et autre pour approximer value function


# es-ce qu'il fonce contre le mur ?? Pourquoi reward ne drop pas ??
# PROBELEMe : Doit mettre un petit reqrd positif a chacun de ses pas, sinon loss fonction ne change pas car reward est toujours = 0 et donc aprend pas
# ce sera fixe avec les images de benj parce que bruit gaussian

# PROBLEME : dans la maniere dont on choisi action 

# has to overfitt the L
# continuous policy -> bilinear interpolation
# KL
# try on images of simple axons 
# git hub issues for KL - gradeint of hessian???
# for inital -> grid of zeros

# differentiating stochastic functions requires providing a reward
# => need to call .reinforce on the stochastic outputs before calling .backward. OR make it volatile ??

# Que-ce qui doit etre une Variable dans le loss?
# avantage devrait etre normalise (a chaque pas de temps, faire ((a - mean(A)) / std(A))

# Pour l'instant on a action et action_2, on doit envoyer action_2 a step environment !!!
# et environnement doit changer x et y en fonction 
# KL
# TRPO step
# continuous policy, avec ecran qui bouge, downsampling

# voir si le loss decroit
# sinon il y a un probleme dans fonction step_trpo
# peut etre que la partie importante etait dans line search??
# loss final apres 1000 iterations est a 1, c'est beaucoup trop haut
# du au fait qu'on arrondit les continuous polices ou probleme dans TRPO step?
# faire continuous d'abord, ensuite on verra avec Kai

# checker avantages et KL
# Regler probleme de la premiere policy

# peut etre que la partie importante etait dans line search??

# verifier que : prev_prev_prev_state = prev_prev_state
# on change bien le truc et ca fait pas l'espece de copie ou ca reste le meme truc

# pourquoi variance = 0 ??? du neural net
# Parce que c'est pas un batch ??

# pourquoi le loss ne change pas du tout ?
