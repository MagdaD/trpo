#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jul 28 16:32:48 2017

@author: magdadubois
"""
import torch
import numpy as np
import torch.autograd as autograd
from torch.autograd import Variable

#######################
######## TRPO #########
#######################

def conjugate_gradients(Avp, b, nsteps, residual_tol=1e-10):
    x = torch.zeros(b.size())
    r = b - Avp(x)
    p = r
    rdotr = torch.dot(r, r)
    
    for i in range(nsteps):
        _Avp = Avp(p)
        alpha = rdotr / torch.dot(p, _Avp)
        x += alpha * p
        r -= alpha * _Avp
        new_rdotr = torch.dot(r, r)
        betta = new_rdotr / rdotr
        p = r + betta * p
        rdotr = new_rdotr
        if rdotr < residual_tol:
            break
    return x


#def trpo_step(model, loss, kl, max_kl, damping):

#grads = autograd.grad(loss, model.parameters(), create_graph=True, retain_graph=True)
#loss_grad = torch.cat([grad.view(-1) for grad in grads]).data

#def Fvp(v):
#    kl_l = kl.mean()
#    grads = autograd.grad(kl_l, model.parameters(), create_graph=True, retain_graph=True)
#    flat_grad_kl = torch.cat([grad.view(-1) for grad in grads])

#    kl_v = (flat_grad_kl * Variable(v)).sum()
#    grads = autograd.grad(kl_v, model.parameters(), create_graph=True, retain_graph=True)
#    flat_grad_grad_kl = torch.cat([grad.view(-1) for grad in grads]).data

#    return flat_grad_grad_kl + v * damping

#stepdir = conjugate_gradients(Fvp, -loss_grad, 10)

#shs = 0.5 * (stepdir * Fvp(stepdir)).sum(0)

#lm = torch.sqrt(shs / max_kl)
#fullstep = stepdir / lm[0]

#neggdotstepdir = (-loss_grad * stepdir).sum(0)
#print(("lagrange multiplier:", lm[0], "grad_norm:", loss_grad.norm()))

def normal_log_density(x, mean, log_std, std):
    
    var = std.pow(2)
    
    log_density = -(x - mean).pow(2) / (2 * var) - 0.5 * math.log(2 * math.pi) - log_std
    
    return log_density.sum(1)

def normal_density(x, mean, std):
    
    var = std.pow(2)
    
    density = 1./torch.sqrt(2.0*np.pi*var) * torch.exp(- ((x-mean)**2) / (2.0 * var))
    
    return density
