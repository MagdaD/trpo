#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jul 28 16:28:59 2017

@author: magdadubois
"""
import torch
import torch.nn as nn
import torch.nn.functional as F

from parameters import *

#######################
##### NeuralNets ######
#######################
class ValueNet(nn.Module):
    def __init__(self, num_inputs):
        super(ValueNet, self).__init__()
        #self.affine1 = nn.Linear(num_inputs*8*8, 64)
        #self.affine2 = nn.Linear(64, num_inputs)
        #self.value_head = nn.Linear(num_inputs, 1)
        #self.value_head.weight.data.mul_(0.1)
        #self.value_head.bias.data.mul_(0.0)
        
        self.affine1 = nn.Linear(num_inputs*8*8, 64)
        self.affine2 = nn.Linear(64, 64)
        self.value_head = nn.Linear(64, num_inputs)
        self.value_head.weight.data.mul_(0.1)
        self.value_head.bias.data.mul_(0.0)
    
    def forward(self, x):
        x = x.view(-1, num_timesteps*8*8)
        x = F.tanh(self.affine1(x))
        x = F.tanh(self.affine2(x))
        
        state_values = self.value_head(x)
        
        return state_values

class PolicyNet(nn.Module):
    def __init__(self):
        super(PolicyNet, self).__init__()
        self.conv1 = nn.Conv2d(6, 16, 3) # Args: input_channels, output_channels, kernel_size
        self.conv2 = nn.Conv2d(16, 5, 3)
        self.fc1 = nn.Linear(5*4*4,8)
        self.softmax = nn.Softmax()
    
    def forward(self, x):
        x = self.conv1(x)
        x = self.conv2(x) #5 output en 2 dimensions, on veut lineariser pour fc, on utilise fonction view
        x = x.view(-1, 5*4*4)
        x = self.fc1(x)
        x = self.softmax(x).clamp(max=1 - eps, min=eps/7) #fixes the fact that some values become nan (A la base 20, essaye avec 15)
        return x

class Policy(nn.Module):
    def __init__(self):
        super(Policy, self).__init__()
        self.conv1 = nn.Conv2d(12, 16, 3) #
        self.conv2 = nn.Conv2d(16, 5, 3) #
        self.affine1 = nn.Linear(5*4*4, 64)
        self.affine2 = nn.Linear(64, 64)
        
        self.action_mean = nn.Linear(64, 2) #avant 8
        self.action_mean.weight.data.mul_(0.1)
        self.action_mean.bias.data.mul_(0.0)
        
        self.action_log_std = nn.Parameter(torch.zeros(1, 2))
        
        self.saved_actions = []
        self.rewards = []
        self.final_value = 0
    
    def forward(self, x):
        x = self.conv1(x)
        
        x = self.conv2(x)
        x = x.view(-1, 5*4*4)
        
        x = F.tanh(self.affine1(x))
        x = F.tanh(self.affine2(x))
        
        action_mean = self.action_mean(x)
        action_log_std = self.action_log_std.expand_as(action_mean)
        action_std = torch.exp(action_log_std)
        
        return action_mean, action_log_std, action_std
