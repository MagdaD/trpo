#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jul 28 15:17:03 2017

@author: magdadubois
"""
import numpy as np
import copy
import torch
from torch.autograd import Variable
import torchvision

from parameters import *

######################
###### The Grid ######
######################

class Grid():
    def __init__(self, grid_size = GRID_SIZE):
        self.grid_size = grid_size
        self.y_disp = 0
        self.x_disp = 0
    
    def reset(self):
        self.padded_size = self.grid_size + 2 * VISIBLE_RADIUS+1
        self.grid = MIN_PIXEL_VALUE + np.zeros((self.padded_size, self.padded_size)) # Padding for edges
        self.full_size = self.grid.shape[0]
        
        # Edges
        self.grid[0:VISIBLE_RADIUS, :] = EDGE_VALUE
        self.grid[-(1*VISIBLE_RADIUS):, :] = EDGE_VALUE
        self.grid[:, 0:VISIBLE_RADIUS] = EDGE_VALUE
        self.grid[:, -(1*VISIBLE_RADIUS):] = EDGE_VALUE
        
        #For the grid with only the previous position
        #self.grid_previous_position = copy.deepcopy(self.grid)
        
        #Maintenant on a donc une matrice grid_size * grid_size avec une bordure de -10 autour
        
        # On cree un L et tout le reste est a 0
        i = round(self.full_size/2) #pour mettre le trait a peu pres au milieu
        
        #vertical line
        self.grid[VISIBLE_RADIUS+int(GRID_SIZE/4):VISIBLE_RADIUS+int(GRID_SIZE/2)+2, i+1] = MAX_PIXEL_VALUE
        
        self.goal_position = (VISIBLE_RADIUS+int(GRID_SIZE/4), i+1)
        
        self.grid[self.goal_position[0]-RADIUS:self.goal_position[0]+RADIUS, self.goal_position[1]-RADIUS:self.goal_position[1]+RADIUS] = GOAL_VALUE #
        
        
        #horizontal line
        self.grid[VISIBLE_RADIUS+int(GRID_SIZE/2)+1,VISIBLE_RADIUS+int(GRID_SIZE/4):i+1] = MAX_PIXEL_VALUE
        
        self.ng = np.zeros((self.padded_size, self.padded_size))
        self.ng_previous_position = copy.deepcopy(self.ng)
    
    def grid_rescale(self, im):
        
        im = torch.from_numpy(im)
        
        sizex = im.size()[0] #testgrid.size
        sizey = im.size()[1]
        
        final_size = VISIBLE_RADIUS #(cense etre la meme taille que la vue normale)
        
        decimal = 100 #Are converted to int when rescaled so we multiply and divide by this number
        
        im3d = torch.IntTensor(1, sizex, sizey)
        
        im3d[0] = im*decimal
        
        trans = torchvision.transforms.Compose([torchvision.transforms.ToPILImage(),
                                                torchvision.transforms.Scale(final_size),
                                                torchvision.transforms.ToTensor()])
        out = trans(im3d).numpy() / decimal
                                                
        out = np.reshape(out, (final_size,final_size))
                                                
        return out

    def visible(self, pos):
        y, x = pos
        vis_rad = int(VISIBLE_RADIUS/2)
        return self.grid[y-vis_rad:y+vis_rad, x-vis_rad:x+vis_rad]
    
    def visible_interpolated(self, pos):
        y,x = pos
        
        vis_rad = int(VISIBLE_RADIUS/2)
        
        N = self.padded_size
        
        all = torch.Tensor(1,1,N,N)
        all[0][0][:][:] = torch.Tensor(self.grid)
        
        y_disp_norm = self.y_disp / N #on ne veut pas que bouge de 0.2 de la grid, 0.2 d'un pixel. Donc on divise par nombre de pixel
        x_disp_norm = self.x_disp / N
        
        # make the xv and yv spaces (2D linspaces)
        y1 = np.linspace(-1, 1, N, endpoint=True)
        x1 = np.linspace(-1, 1, N, endpoint=True)
        yv, xv = np.meshgrid(y1, x1)
        
        #Specidy the displacement, and we create a 4D tensor with it
        ycord = yv + y_disp_norm
        xcord = xv + x_disp_norm
        pix = torch.Tensor(1, N, N, 2)
        for i in range(N):
            for j in range(N):
                pix[0][i][j][0]= torch.Tensor(ycord)[i][j]
                pix[0][i][j][1] = torch.Tensor(xcord)[i][j]
        
        newgrid = torch.nn.functional.grid_sample(Variable(all), Variable(pix))
        
        for i in range(N):
            for j in range(N):
                self.ng[i][j] = float(newgrid[0][0][i][j].data.numpy())

        return self.ng[y-vis_rad:y+vis_rad, x-vis_rad:x+vis_rad]
                        
                        #has to be called after visible_interpolated
    def visible_interpolated_larger(self, pos):
        y,x = pos
        return self.ng[y-VISIBLE_RADIUS:y+VISIBLE_RADIUS, x-VISIBLE_RADIUS:x+VISIBLE_RADIUS]
    
    def visible_interpolated_scaled(self, pos):
        y,x = pos
        im = self.ng[y-VISIBLE_RADIUS:y+VISIBLE_RADIUS, x-VISIBLE_RADIUS:x+VISIBLE_RADIUS]
        return self.grid_rescale(im)
    
    #DOWNSCALE
    def visible_larger(self, pos):
        y,x = pos
        return self.grid[y-VISIBLE_RADIUS:y+VISIBLE_RADIUS, x-VISIBLE_RADIUS:x+VISIBLE_RADIUS]
    
    def visible_scaled(self, pos):
        y,x = pos
        im = self.grid[y-VISIBLE_RADIUS:y+VISIBLE_RADIUS, x-VISIBLE_RADIUS:x+VISIBLE_RADIUS]
        return self.grid_rescale(im)
    
    #PREVIOUS POSITION
    def visible_previous_pos(self, pos):
        y,x = pos
        vis_rad = int(VISIBLE_RADIUS/2)
        return self.grid_previous_position[y-vis_rad:y+vis_rad, x-vis_rad:x+vis_rad]
    
    def visible_interpolated_previous_pos(self, pos):
        y,x = pos
        vis_rad = int(VISIBLE_RADIUS/2)
        return self.ng_previous_position[y-vis_rad:y+vis_rad, x-vis_rad:x+vis_rad]
