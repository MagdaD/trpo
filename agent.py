#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jul 28 15:18:25 2017

@author: magdadubois
"""
#######################
###### The Agent ######
#######################

import numpy as np
import torch
from torch.autograd import Variable

# All this class does is update the position based on an action (up, right, down or left)

class Agent:
    def reset(self):
        self.reward = 0

    # Discrete action
    def act(self, action):
        # Move according to action: 0=UP, 1=RIGHT, 2=DOWN, 3=LEFT
        y, x = self.pos

        if action.data.numpy()[0] == 0: 
            y -= 1
        elif action.data.numpy()[0] == 1: 
            y -= 1 
            x += 1
        elif action.data.numpy()[0] == 2:
            x += 1   
        elif action.data.numpy()[0] == 3: 
            x += 1 
            y += 1
        elif action.data.numpy()[0] == 4: 
            y += 1
        elif action.data.numpy()[0] == 5:
            y += 1 
            x -= 1      
        elif action.data.numpy()[0] == 6:
            x -= 1 
        elif action.data.numpy()[0] == 7:
            x -= 1 
            y -= 1
        
        self.pos = (y, x)
        
        
    # Continuous action   
    def act_cont(self, action):
        
        ycont = self.cont_pos.data[0]
        xcont = self.cont_pos.data[1]
        
        ycont+=action.data[0]
        xcont+=action.data[1]
        
        #position is set to the closest pixel first
        pixel_y = int(np.round(ycont))
        pixel_x = int(np.round(xcont))
        
        self.pos = (pixel_y, pixel_x)
        
        self.cont_pos = Variable(torch.from_numpy(np.array([ycont,xcont])))
