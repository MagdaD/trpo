#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jul 28 16:31:49 2017

@author: magdadubois
"""

import torch
import numpy as np
from torch.autograd import Variable

#######################
###### Advantage ######
#######################
GAMMA = 0.1
LAMBDA = 1

def comp_advantage(all_rewards, all_done, all_states, value_model, optimiser_val):
    
    rewards = torch.squeeze(torch.Tensor(all_rewards)) # timestep, batch
    masks = torch.squeeze(torch.Tensor(all_done)) # timestep, batch
    states = torch.squeeze(torch.Tensor(all_states)) # timestep x batch x 8 x 8
    
    value_function = value_model(Variable(states)) # batch x timestep
    t_value_function = value_function.transpose(0,1) # timestep x batch
    
    returns = torch.Tensor(rewards.size(0), rewards.size(1)) # timestep, batch
    deltas = torch.Tensor(rewards.size(0), rewards.size(1))
    advantages = torch.Tensor(rewards.size(0), rewards.size(1))
    
    prev_return = torch.Tensor(np.zeros(rewards.size(1)))
    prev_value = torch.Tensor(np.zeros(rewards.size(1)))
    prev_advantage = torch.Tensor(np.zeros(rewards.size(1)))
    
    
    for i in reversed(range(rewards.size(0))):
        
        returns[i] = rewards[i]+ GAMMA * prev_return * masks[i]
        
        deltas[i] = rewards[i] - prev_value + GAMMA * t_value_function.data[i] * masks[i]
        advantages[i] = deltas[i] + GAMMA * LAMBDA * prev_advantage * masks[i]
        
        prev_return = returns[i]
        prev_value = t_value_function.data[i]
        prev_advantage = advantages[i]
    
    targets = Variable(returns)

    value_loss = (t_value_function - targets).pow(2).mean()
    
    for param in value_model.parameters():
        value_loss += param.pow(2).sum() * 1e-3
    
    value_loss.backward()
    optimiser_val.step()
    
    return advantages
